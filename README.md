class Solution:
    def kClosest(self, points: List[List[int]], K: int) -> List[List[int]]:
        points = sorted(points,key = lambda i : i[0]*i[0] + i[1]*i[1])
        return points[:K]
        
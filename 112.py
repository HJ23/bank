def printArr(array):
    temp=""
    for x in range(0,len(array)):
        for y in range(0,len(array[0])):
            temp+=str(array[x][y])+" "
        temp+="\n"
    print(temp)

def shuffle(row,column):
    array=[]
    temp=[]
    for x in range(0,row*column):
        COL=x%column
        temp.append(x)
        if(COL==column-1):
            array.append(temp)
            temp=[]
    
    for x in range(0,row//2+1):
        for y in range(0,column,2):
            if(x==row//2):
                temp=array[x][y]
                array[x][y]=array[0][column-y-1]
                array[0][column-y-1]=temp
            else:
                temp=array[x][y]
                array[x][y]=array[row-1-x][column-y-1]
                array[row-1-x][column-y-1]=temp
        
    
    printArr(array)

shuffle(3,4)